TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_LIBS = -lboost_regex -lboost_filesystem -lboost_system

DEFINES += "PROJ_DIR=\"\\\"$$PWD\\\"\""

SOURCES += main.cpp \
    add_dependent_lib.cpp \
    add_path.cpp \
    bcp_imp.cpp \
    copy_path.cpp \
    file_types.cpp \
    fileview.cpp \
    licence_info.cpp \
    output_licence_info.cpp \
    path_operations.cpp \
    scan_cvs_path.cpp \
    scan_licence.cpp

HEADERS += \
    bcp.hpp \
    bcp_imp.hpp \
    fileview.hpp \
    licence_info.hpp

DISTFILES += \
    manual_copyright
