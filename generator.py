#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import json

def write_stanza(data):
    name = data["key"]
    authors = data["authors"]
    print("Files:")
    print(" libs/" + name + "/*")
    print("Copyright:")
    for auth in authors:
        print(" " + auth)
    print("License: BSL-1.0")
    print()

def main():
    for line in sys.stdin:
        line = line.strip()
        with open(line) as fin:
            data = json.load(fin)
        try:
            write_stanza(data)
        except:
            for datum in data:
                write_stanza(datum)

if __name__ == '__main__':
    main()
