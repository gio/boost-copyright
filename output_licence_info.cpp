/*
 *
 * Copyright (c) 2003 Dr John Maddock
 * Use, modification and distribution is subject to the 
 * Boost Software License, Version 1.0. (See accompanying file 
 * LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 *
 */

#include "licence_info.hpp"
#include "bcp_imp.hpp"
#include "fileview.hpp"
#include <fstream>
#include <iomanip>
#include <cstring>
#include <stdexcept>
#include <boost/lexical_cast.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/throw_exception.hpp>
#include <boost/filesystem/fstream.hpp>
#include <iostream>

// Hint: use sed -e 's|^$|.|g' -e 's|"|\\"|g' -e 's|^|" |g' -e 's|$|\\n"|g'
std::map< std::string, std::tuple< std::string, std::string > > licenses_short_names = {
    { "Boost Software License, Version 1.0", { "BSL-1.0",
                                               " Permission is hereby granted, free of charge, to any person or organization\n"
                                               " obtaining a copy of the software and accompanying documentation covered by\n"
                                               " this license (the \"Software\") to use, reproduce, display, distribute,\n"
                                               " execute, and transmit the Software, and to prepare derivative works of the\n"
                                               " Software, and to permit third-parties to whom the Software is furnished to\n"
                                               " do so, all subject to the following:\n"
                                               " .\n"
                                               " The copyright notices in the Software and this entire statement, including\n"
                                               " the above license grant, this restriction and the following disclaimer,\n"
                                               " must be included in all copies of the Software, in whole or in part, and\n"
                                               " all derivative works of the Software, unless such copies or derivative\n"
                                               " works are solely in the form of machine-executable object code generated by\n"
                                               " a source language processor.\n"
                                               " .\n"
                                               " THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR\n"
                                               " IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\n"
                                               " FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT\n"
                                               " SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE\n"
                                               " FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,\n"
                                               " ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER\n"
                                               " DEALINGS IN THE SOFTWARE.\n" } },
    { "Python Software License", { "PSF",
                                   " 1. This LICENSE AGREEMENT is between the Python Software Foundation\n"
                                   " (\"PSF\"), and the Individual or Organization (\"Licensee\") accessing and\n"
                                   " otherwise using Python 2.2 software in source or binary form and its\n"
                                   " associated documentation.\n"
                                   " .\n"
                                   " 2. Subject to the terms and conditions of this License Agreement, PSF\n"
                                   " hereby grants Licensee a nonexclusive, royalty-free, world-wide\n"
                                   " license to reproduce, analyze, test, perform and/or display publicly,\n"
                                   " prepare derivative works, distribute, and otherwise use Python 2.2\n"
                                   " alone or in any derivative version, provided, however, that PSF's\n"
                                   " License Agreement and PSF's notice of copyright, i.e., \"Copyright (c)\n"
                                   " 2001 Python Software Foundation; All Rights Reserved\" are retained in\n"
                                   " Python 2.2 alone or in any derivative version prepared by Licensee.\n"
                                   " .\n"
                                   " 3. In the event Licensee prepares a derivative work that is based on\n"
                                   " or incorporates Python 2.2 or any part thereof, and wants to make\n"
                                   " the derivative work available to others as provided herein, then\n"
                                   " Licensee hereby agrees to include in any such work a brief summary of\n"
                                   " the changes made to Python 2.2.\n"
                                   " .\n"
                                   " 4. PSF is making Python 2.2 available to Licensee on an \"AS IS\"\n"
                                   " basis.  PSF MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR\n"
                                   " IMPLIED.  BY WAY OF EXAMPLE, BUT NOT LIMITATION, PSF MAKES NO AND\n"
                                   " DISCLAIMS ANY REPRESENTATION OR WARRANTY OF MERCHANTABILITY OR FITNESS\n"
                                   " FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF PYTHON 2.2 WILL NOT\n"
                                   " INFRINGE ANY THIRD PARTY RIGHTS.\n"
                                   " .\n"
                                   " 5. PSF SHALL NOT BE LIABLE TO LICENSEE OR ANY OTHER USERS OF PYTHON\n"
                                   " 2.2 FOR ANY INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS AS\n"
                                   " A RESULT OF MODIFYING, DISTRIBUTING, OR OTHERWISE USING PYTHON 2.2,\n"
                                   " OR ANY DERIVATIVE THEREOF, EVEN IF ADVISED OF THE POSSIBILITY THEREOF.\n"
                                   " .\n"
                                   " 6. This License Agreement will automatically terminate upon a material\n"
                                   " breach of its terms and conditions.\n"
                                   " .\n"
                                   " 7. Nothing in this License Agreement shall be deemed to create any\n"
                                   " relationship of agency, partnership, or joint venture between PSF and\n"
                                   " Licensee.  This License Agreement does not grant permission to use PSF\n"
                                   " trademarks or trade name in a trademark sense to endorse or promote\n"
                                   " products or services of Licensee, or any third party.\n"
                                   " .\n"
                                   " 8. By copying, installing or otherwise using Python 2.2, Licensee\n"
                                   " agrees to be bound by the terms and conditions of this License\n"
                                   " Agreement.\n" } },
    { "SGI Style License", { "SGI",
                             " Permission to use, copy, modify, distribute and sell this software\n"
                             " and its documentation for any purpose is hereby granted without fee,\n"
                             " provided that the above copyright notice appear in all copies and\n"
                             " that both that copyright notice and this permission notice appear\n"
                             " in supporting documentation.  The authors make no\n"
                             " representations about the suitability of this software for any\n"
                             " purpose.  It is provided \"as is\" without express or implied warranty.\n" } },
    { "Old style Boost license #2", { "OldBoost1",
                                      " Permission to copy, use, modify, sell and distribute this software\n"
                                      " is granted provided this copyright notice appears in all copies.\n"
                                      " This software is provided \"as is\" without express or implied\n"
                                      " warranty, and with no claim as to its suitability for any purpose.\n" } },
    { "Old style Boost license #4", { "OldBoost2",
                                      " Permission to copy, use, sell and distribute this software is granted\n"
                                      " provided this copyright notice appears in all copies.\n"
                                      " Permission to modify the code and to distribute modified code is granted\n"
                                      " provided this copyright notice appears in all copies, and a notice\n"
                                      " that the code was modified is included with the copyright notice.\n"
                                      " .\n"
                                      " This software is provided \"as is\" without express or implied warranty,\n"
                                      " and with no claim as to its suitability for any purpose.\n" } },
    { "Old style Boost license #5", { "OldBoost3",
                                      " Permission to copy, use, modify, sell and distribute this software is\n"
                                      " granted, provided this copyright notice appears in all copies and\n"
                                      " modified version are clearly marked as such. This software is provided\n"
                                      " \"as is\" without express or implied warranty, and with no claim as to its\n"
                                      " suitability for any purpose.\n" } },
    { "Old style Boost license #8" , { "OldBoost4",
                                       " This software is provided 'as-is', without any express or implied\n"
                                       " warranty. In no event will the copyright holder be held liable for\n"
                                       " any damages arising from the use of this software.\n"
                                       " .\n"
                                       " Permission is granted to anyone to use this software for any purpose,\n"
                                       " including commercial applications, and to alter it and redistribute\n"
                                       " it freely, subject to the following restrictions:\n"
                                       " .\n"
                                       " 1.  The origin of this software must not be misrepresented; you must\n"
                                       " not claim that you wrote the original software. If you use this\n"
                                       " software in a product, an acknowledgment in the product documentation\n"
                                       " would be appreciated but is not required.\n"
                                       " .\n"
                                       " 2.  Altered source versions must be plainly marked as such, and must\n"
                                       " not be misrepresented as being the original software.\n"
                                       " .\n"
                                       " 3.  This notice may not be removed or altered from any source\n"
                                       " distribution.\n" } },
    { "Perforce Jam License", { "Perforce",
                                " License is hereby granted to use this software and distribute it\n"
                                " freely, as long as this copyright notice is retained and modifications\n"
                                " are clearly marked.\n"
                                " .\n"
                                " ALL WARRANTIES ARE HEREBY DISCLAIMED.\n"} },
    { "BSD Regex License", { "BSDRegex",
                             " Permission is granted to anyone to use this software for any\n"
                             " purpose on any computer system, and to redistribute it freely,\n"
                             " subject to the following restrictions:\n"
                             " .\n"
                             " 1. The author is not responsible for the consequences of use of\n"
                             "    this software, no matter how awful, even if they arise\n"
                             "    from defects in it.\n"
                             " .\n"
                             " 2. The origin of this software must not be misrepresented, either\n"
                             "    by explicit claim or by omission.\n"
                             " .\n"
                             " 3. Altered versions must be plainly marked as such, and must not\n"
                             "    be misrepresented as being the original software.\n" } },
    { "GNU Parser Licence", { "GPL-2+ with Bison exception",
                              " This program is free software; you can redistribute it and/or modify\n"
                              " it under the terms of the GNU General Public License as published by\n"
                              " the Free Software Foundation; either version 2, or (at your option)\n"
                              " any later version.\n"
                              " .\n"
                              " This program is distributed in the hope that it will be useful,\n"
                              " but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
                              " MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
                              " GNU General Public License for more details. On Debian systems\n"
                              " a complete copy of the GPL license is available in\n"
                              " /usr/share/common-licenses/GPL.\n"
                              " .\n"
                              " As a special exception, when this file is copied by Bison into a\n"
                              " Bison output file, you may use that output file without restriction.\n"
                              " This special exception was added by the Free Software Foundation\n"
                              " in version 1.24 of Bison.\n" } },
    { "Public Domain", { "PD",
                         " This file has explicitly been place in the public domain.\n" } },
};

std::vector< boost::regex > ignore_regexes = {
    boost::regex("debian/.*"),
    boost::regex("\\.pc/.*"),

    /*boost::regex("tools/quickbook/test/.*"),
    boost::regex("libs/graph/test/planar_input_graphs/.*"),
    boost::regex("doc/html/images/.*"),
    boost::regex("doc/src/images/.*"),
    boost::regex("libs/.* /meta/libraries.json"),
    boost::regex("libs/algorithm/sublibs"),
    boost::regex("libs/algorithm/test/search_test_data/.*"),*/
};

bool ignore_path(const std::string &p) {
    for (const auto &re : ignore_regexes) {
        if (boost::regex_match(p.begin(), p.end(), re)) {
            return true;
        }
    }
    return false;
}

std::string glob_to_regex(const std::string &s) {
    std::string ret;
    for (auto it = s.begin()+1; it != s.end(); it++) {
        if (*it == '*') {
            ret.push_back('.');
            ret.push_back('*');
        } else {
            ret.push_back(*it);
        }
    }
    return ret;
}

//
// split_path is a small helper for outputting a path name, 
// complete with a link to that path:
//
struct split_path
{
   const fs::path& root;
   const fs::path& file;
   split_path(const fs::path& r, const fs::path& f)
      : root(r), file(f){}
private:
   split_path& operator=(const split_path&);
};

std::ostream& operator << (std::ostream& os, const split_path& p)
{
   os << "<a href=\"" << (p.root / p.file).string() << "\">" << p.file.string() << "</a>";
   return os;
}

std::string make_link_target(const std::string& s)
{
   // convert an arbitrary string into something suitable
   // for an <a> name:
   std::string result;
   for(unsigned i = 0; i < s.size(); ++i)
   {
      result.append(1, static_cast<std::string::value_type>(std::isalnum(s[i]) ? s[i] : '_'));
   }
   return result;
}

void bcp_implementation::output_authors() {
    std::ofstream os(m_dest_path.string().c_str());
    if(!os)
    {
       std::string msg("Error opening ");
       msg += m_dest_path.string();
       msg += " for output.";
       std::runtime_error e(msg);
       boost::throw_exception(e);
    }

    std::set< std::string > authors;
    for (const auto &p : m_file_data) {
        for (const auto &author : p.second.authors) {
            authors.insert(author);
        }
    }

    for (const auto &author : authors) {
        os << author << std::endl;
    }
}

void bcp_implementation::output_debian_copyright() {
    std::pair<const license_info*, int> licenses = get_licenses();
    std::ofstream os(m_dest_path.string().c_str());
    if(!os)
    {
       std::string msg("Error opening ");
       msg += m_dest_path.string();
       msg += " for output.";
       std::runtime_error e(msg);
       boost::throw_exception(e);
    }
    //os << "\xef\xbb\xbf";

    // Small fixup: tools/bcp/licence_info.cpp contains many license templates
    // and confuses the license scanner; here we fix it
    this->m_file_data.at(fs::path("tools/bcp/licence_info.cpp")).authors = { "John Maddock" };
    this->m_file_data.at(fs::path("tools/bcp/licence_info.cpp")).licenses = { "Boost Software License, Version 1.0" };

    boost::filesystem::ifstream fmanual(boost::filesystem::path(PROJ_DIR) / "manual_copyright");
    std::string line;
    bool files = false;
    while (std::getline(fmanual, line)) {
        os << line << std::endl;
        if (files) {
            if (line[0] == ' ') {
                ignore_regexes.push_back(boost::regex(glob_to_regex(line)));
            } else {
                files = false;
            }
        }
        if (line == std::string("Files:")) {
            files = true;
        }
    }

    std::map< file_data, std::set< fs::path, path_less > > inv_file_data;
    std::vector< std::pair< file_data, std::set< fs::path, path_less > > > file_data;
    for (const auto &p : m_file_data) {
        inv_file_data[p.second].insert(p.first);
        file_data.push_back(std::make_pair(p.second, std::set< fs::path, path_less >{p.first}));
    }

    std::set< std::string > authors;
    //std::cout << "\xef\xbb\xbf";
    std::cout << "Files with missing info:" << std::endl;
    std::set< std::string > used_licenses;
    // Comment one or the other line to group by identicaly licenses or not
    for (const auto &p : inv_file_data) {
    //for (const auto &p : file_data) {
        if (p.first.authors.empty() || p.first.licenses.empty()) {
            for (const auto &path : p.second) {
                auto path_str = path.string();
                if (ignore_path(path_str)) {
                    continue;
                }
                std::replace(path_str.begin(), path_str.end(), ' ', '.');
                std::cout << " " << path_str << " [";
                std::cout << (p.first.authors.empty() ? "C" : "");
                std::cout << (p.first.licenses.empty() ? "L" : "");
                std::cout << "]" << std::endl;
            }
            continue;
        }
        os << std::endl;
        os << "Files:" << std::endl;
        for (const auto &path : p.second) {
            auto path_str = path.string();
            std::replace(path_str.begin(), path_str.end(), ' ', '.');
            os << " " << path_str << std::endl;
        }
        os << "Copyright:" << std::endl;
        for (const auto &author : p.first.authors) {
            os << " " << author << std::endl;
            authors.insert(author);
        }
        os << "License: ";
        bool first_lic = true;
        for (const auto &license : p.first.licenses) {
            if (first_lic) {
                first_lic = false;
            } else {
                os << " and ";
            }
            os << std::get< 0 >(licenses_short_names.at(license));
            used_licenses.insert(license);
        }
        os << std::endl;
    }

    for (const auto &lic : used_licenses) {
        os << std::endl;
        os << "License: " << std::get< 0 >(licenses_short_names.at(lic)) << std::endl;
        os << std::get< 1 >(licenses_short_names.at(lic));
    }

    std::cout << "All authors:" << std::endl;
    for (const auto &author : authors) {
        std::cout << " " << author << std::endl;
    }

    // Write on non-BSL licenses
    if (false) {
        std::cout << std::endl;
        for (const auto &lic : m_license_data) {
            std::string bsl = "Boost Software License, Version 1.0";
            if (!std::equal(bsl.begin(), bsl.end(), licenses.first[lic.first].license_name.begin())) {
                if (!lic.second.files.empty()) {
                    std::cout << "Files under " << licenses.first[lic.first].license_name << ":" << std::endl;
                    for (const auto &f : lic.second.files) {
                        std::cout << "  " << f.string() << std::endl;
                    }
                    std::cout << std::endl;
                }
            }
        }
    }
}

void bcp_implementation::output_license_info()
{
   std::pair<const license_info*, int> licenses = get_licenses();

   std::map<int, license_data>::const_iterator i, j;
   i = m_license_data.begin();
   j = m_license_data.end();

   std::ofstream os(m_dest_path.string().c_str());
   if(!os)
   {
      std::string msg("Error opening ");
      msg += m_dest_path.string();
      msg += " for output.";
      std::runtime_error e(msg);
      boost::throw_exception(e);
   }
   os << 
      "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n"
      "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n"
      "<html>\n"
      "<head>\n"
      "<title>Boost Licence Dependency Information";
   if(m_module_list.size() == 1)
   {
      os << " for " << *(m_module_list.begin());
   }
   os << 
      "</title>\n"
      "</head>\n"
      "<body>\n"
      "<H1>Boost Licence Dependency Information";
   if(m_module_list.size() == 1)
   {
      os << " for " << *(m_module_list.begin());
   }
   os << 
      "</H1>\n"
      "<H2>Contents</h2>\n"
      "<pre><a href=\"#input\">Input Information</a>\n";
   if(!m_bsl_summary_mode)
      os << "<a href=\"#summary\">Licence Summary</a>\n";
   os << "<a href=\"#details\">Licence Details</a>\n";

   while(i != j)
   {
      // title:
      os << "   <A href=\"#" << make_link_target(licenses.first[i->first].license_name) 
         << "\">" << licenses.first[i->first].license_name << "</a>\n";
      ++i;
   }

   os << "<a href=\"#files\">Files with no recognised license</a>\n"
      "<a href=\"#authors\">Files with no recognised copyright holder</a>\n";
   if(!m_bsl_summary_mode)
   {
      os <<
      "Moving to the Boost Software License...\n"
      "  <a href=\"#bsl-converted\">Files that can be automatically converted to the Boost Software License</a>\n"
      "  <a href=\"#to-bsl\">Files that can be manually converted to the Boost Software License</a>\n"
      "  <a href=\"#not-to-bsl\">Files that can <b>NOT</b> be moved to the Boost Software License</a>\n"
      "  <a href=\"#need-bsl-authors\">Authors we need to move to the Boost Software License</a>\n"
      "<a href=\"#copyright\">Copyright Holder Information</a>\n";
   }
   os << 
      "<a href=\"#depend\">File Dependency Information</a>\n"
      "</pre>";

   //
   // input Information:
   //
   os << "<a name=\"input\"></a><h2>Input Information</h2>\n";
   if(m_scan_mode)
      os << "<P>The following files were scanned for boost dependencies:<BR>";
   else
      os << "<P>The following Boost modules were checked:<BR>";

   std::list<std::string>::const_iterator si = m_module_list.begin();
   std::list<std::string>::const_iterator sj = m_module_list.end();
   while(si != sj)
   {
      os << *si << "<BR>";
      ++si;
   }
   os << "</p><p>The Boost path was: <code>" << m_boost_path.string() << "</code></P>";
   //
   // extract the boost version number from the boost directory tree, 
   // not from this app (which may have been built from a previous
   // version):
   //
   /*fileview version_file(m_boost_path / "boost/version.hpp");
   static const boost::regex version_regex(
      "^[[:blank:]]*#[[:blank:]]*define[[:blank:]]+BOOST_VERSION[[:blank:]]+(\\d+)");
   boost::cmatch what;
   if(boost::regex_search(version_file.begin(), version_file.end(), what, version_regex))
   {
      int version = boost::lexical_cast<int>(what.str(1));
      os << "<p>The Boost version is: " << version / 100000 << "." << version / 100 % 1000 << "." << version % 100 << "</P>\n";
   }*/

   //
   // output each license:
   //
   i = m_license_data.begin();
   j = m_license_data.end();
   if(!m_bsl_summary_mode)
   {
      //
      // start with the summary:
      //
      os << "<a name=\"summary\"></a><h2>Licence Summary</h2>\n";
      while(i != j)
      {
         // title:
         os << 
            "<H3>" << licenses.first[i->first].license_name << "</H3>\n";
         // license text:
         os << "<BLOCKQUOTE>" << licenses.first[i->first].license_text << "</BLOCKQUOTE>";
         // Copyright holders:
         os << "<P>This license is used by " << i->second.authors.size() 
            << " authors and " << i->second.files.size() 
            << " files <a href=\"#" << make_link_target(licenses.first[i->first].license_name) << "\">(see details)</a>";
         os << "</P></BLOCKQUOTE>\n";
         ++i;
      }
   }
   //
   // and now the details:
   //
   i = m_license_data.begin();
   j = m_license_data.end();
   int license_index = 0;
   os << "<a name=\"details\"></a><h2>Licence Details</h2>\n";
   while(i != j)
   {
      // title:
      os << 
         "<H3><A name=\"" << make_link_target(licenses.first[i->first].license_name) 
         << "\"></a>" << licenses.first[i->first].license_name << "</H3>\n";
      // license text:
      os << "<BLOCKQUOTE>" << licenses.first[i->first].license_text << "</BLOCKQUOTE>";
      if(!m_bsl_summary_mode || (license_index >= 3))
      {
         // Copyright holders:
         os << "<P>This license is used by the following " << i->second.authors.size() << " copyright holders:</P>\n<BLOCKQUOTE><P>";
         std::set<std::string>::const_iterator x, y;
         x = i->second.authors.begin();
         y = i->second.authors.end();
         while(x != y)
         {
            os << *x << "<BR>\n";
            ++x;
         }
         os << "</P></BLOCKQUOTE>\n";
         // Files using this license:
         os << "<P>This license applies to the following " << i->second.files.size() << " files:</P>\n<BLOCKQUOTE><P>";
         std::set<fs::path, path_less>::const_iterator m, n;
         m = i->second.files.begin();
         n = i->second.files.end();
         while(m != n)
         {
            os << split_path(m_boost_path, *m) << "<br>\n";
            ++m;
         }
         os << "</P></BLOCKQUOTE>\n";
      }
      else
      {
         os << "<P>This license is used by " << i->second.authors.size() << " authors (list omitted for brevity).</P>\n";
         os << "<P>This license applies to " << i->second.files.size() << " files (list omitted for brevity).</P>\n";
      }
      ++license_index;
      ++i;
   }
   //
   // Output list of files not found to be under license control:
   //
   os << "<h2><a name=\"files\"></a>Files With No Recognisable Licence</h2>\n"
      "<P>The following " << m_unknown_licenses.size() << " files had no recognisable license information:</P><BLOCKQUOTE><P>\n";
   std::set<fs::path, path_less>::const_iterator i2, j2;
   i2 = m_unknown_licenses.begin();
   j2 = m_unknown_licenses.end();
   while(i2 != j2)
   {
      os << split_path(m_boost_path, *i2) << "<br>\n";
      ++i2;
   }
   os << "</p></BLOCKQUOTE>";
   //
   // Output list of files with no found copyright holder:
   //
   os << "<h2><a name=\"authors\"></a>Files With No Recognisable Copyright Holder</h2>\n"
      "<P>The following " << m_unknown_authors.size() << " files had no recognisable copyright holder:</P>\n<BLOCKQUOTE><P>";
   i2 = m_unknown_authors.begin();
   j2 = m_unknown_authors.end();
   while(i2 != j2)
   {
      os << split_path(m_boost_path, *i2) << "<br>\n";
      ++i2;
   }
   os << "</p></BLOCKQUOTE>";
   if(!m_bsl_summary_mode)
   {
      //
      // Output list of files that have been moved over to the Boost
      // Software License, along with enough information for human
      // verification.
      //
      os << "<h2><a name=\"bsl-converted\"></a>Files that can be automatically converted to the Boost Software License</h2>\n"
         << "<P>The following " << m_converted_to_bsl.size() << " files can be automatically converted to the Boost Software License, but require manual verification before they can be committed to CVS:</P>\n";
      if (!m_converted_to_bsl.empty()) 
      {
         typedef std::map<fs::path, std::pair<std::string, std::string>, path_less>
            ::const_iterator conv_iterator;
         conv_iterator i = m_converted_to_bsl.begin(), 
                        ie = m_converted_to_bsl.end();
         int file_num = 1;
         while (i != ie) 
         {
            os << "<P>[" << file_num << "] File: <tt>" << split_path(m_boost_path, i->first) 
               << "</tt><br>\n<table border=\"1\">\n  <tr>\n    <td><pre>" 
               << i->second.first << "</pre></td>\n    <td><pre>"
               << i->second.second << "</pre></td>\n  </tr>\n</table>\n";
            ++i;
            ++file_num;
         }
      }
      //
      // Output list of files that could be moved over to the Boost Software License
      //
      os << "<h2><a name=\"to-bsl\"></a>Files that could be converted to the Boost Software License</h2>\n"
      "<P>The following " << m_can_migrate_to_bsl.size() << " files could be manually converted to the Boost Software License, but have not yet been:</P>\n<BLOCKQUOTE><P>";
      i2 = m_can_migrate_to_bsl.begin();
      j2 = m_can_migrate_to_bsl.end();
      while(i2 != j2)
      {
         os << split_path(m_boost_path, *i2) << "<br>\n";
         ++i2;
      }
      os << "</p></BLOCKQUOTE>";
      //
      // Output list of files that can not be moved over to the Boost Software License
      //
      os << "<h2><a name=\"not-to-bsl\"></a>Files that can NOT be converted to the Boost Software License</h2>\n"
      "<P>The following " << m_cannot_migrate_to_bsl.size() << " files cannot be converted to the Boost Software License because we need the permission of more authors:</P>\n<BLOCKQUOTE><P>";
      i2 = m_cannot_migrate_to_bsl.begin();
      j2 = m_cannot_migrate_to_bsl.end();
      while(i2 != j2)
      {
         os << split_path(m_boost_path, *i2) << "<br>\n";
         ++i2;
      }
      os << "</p></BLOCKQUOTE>";
      //
      // Output list of authors that we need permission for to move to the BSL
      //
      os << "<h2><a name=\"need-bsl-authors\"></a>Authors we need for the BSL</h2>\n"
         "<P>Permission of the following authors is needed before we can convert to the Boost Software License. The list of authors that have given their permission is contained in <code>more/blanket-permission.txt</code>.</P>\n<BLOCKQUOTE><P>";
      std::copy(m_authors_for_bsl_migration.begin(), m_authors_for_bsl_migration.end(),
               std::ostream_iterator<std::string>(os, "<br>\n"));
      os << "</p></BLOCKQUOTE>";
      //
      // output a table of copyright information:
      //
      os << "<H2><a name=\"copyright\"></a>Copyright Holder Information</H2><table border=\"1\">\n";
      std::map<std::string, std::set<fs::path, path_less> >::const_iterator ad, ead; 
      ad = m_author_data.begin();
      ead = m_author_data.end();
      while(ad != ead)
      {
         os << "<tr><td>" << ad->first << "</td><td>";
         std::set<fs::path, path_less>::const_iterator fi, efi;
         fi = ad->second.begin();
         efi = ad->second.end();
         while(fi != efi)
         {
            os << split_path(m_boost_path, *fi) << " ";
            ++fi;
         }
         os << "</td></tr>\n";
         ++ad;
      }
      os << "</table>\n";
   }

   //
   // output file dependency information:
   //
#if 0
   os << "<H2><a name=\"depend\"></a>File Dependency Information</H2><BLOCKQUOTE><pre>\n";
   std::map<fs::path, fs::path, path_less>::const_iterator dep, last_dep;
   std::set<fs::path, path_less>::const_iterator fi, efi;
   fi = m_copy_paths.begin();
   efi = m_copy_paths.end();
   // if in summary mode, just figure out the "bad" files and print those only:
   std::set<fs::path, path_less> bad_paths;
   if(m_bsl_summary_mode)
   {
      bad_paths.insert(m_unknown_licenses.begin(), m_unknown_licenses.end());
      bad_paths.insert(m_unknown_authors.begin(), m_unknown_authors.end());
      bad_paths.insert(m_can_migrate_to_bsl.begin(), m_can_migrate_to_bsl.end());
      bad_paths.insert(m_cannot_migrate_to_bsl.begin(), m_cannot_migrate_to_bsl.end());
      typedef std::map<fs::path, std::pair<std::string, std::string>, path_less>
         ::const_iterator conv_iterator;
      conv_iterator i = m_converted_to_bsl.begin(), 
                     ie = m_converted_to_bsl.end();
      while(i != ie)
      {
         bad_paths.insert(i->first);
         ++i;
      }
      fi = bad_paths.begin();
      efi = bad_paths.end();
      os << "<P>For brevity, only files not under the BSL are shown</P>\n";
   }
   while(fi != efi)
   {
      os << split_path(m_boost_path, *fi);
      dep = m_dependencies.find(*fi);
      last_dep = m_dependencies.end();
      std::set<fs::path, path_less> seen_deps;
      if (dep != last_dep) 
        while(true)
          {
            os << " -> ";
            if(fs::exists(m_boost_path / dep->second))
              os << split_path(m_boost_path, dep->second);
            else if(fs::exists(dep->second))
              os << split_path(fs::path(), dep->second);
            else
              os << dep->second.string();
            if(seen_deps.find(dep->second) != seen_deps.end())
              {
                os << " <I>(Circular dependency!)</I>";
                break; // circular dependency!!!
              }
            seen_deps.insert(dep->second);
            last_dep = dep;
            dep = m_dependencies.find(dep->second);
            if((dep == m_dependencies.end()) || (0 == compare_paths(dep->second, last_dep->second)))
              break;
          }
      os << "\n";
      ++fi;
   }
   os << "</pre></BLOCKQUOTE>\n";
#endif

   os << "</body></html>\n";

   if(!os)
   {
      std::string msg("Error writing to ");
      msg += m_dest_path.string();
      msg += ".";
      std::runtime_error e(msg);
      boost::throw_exception(e);
   }

}
