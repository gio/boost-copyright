/*
 *
 * Copyright (c) 2003 Dr John Maddock
 * Use, modification and distribution is subject to the
 * Boost Software License, Version 1.0. (See accompanying file
 * LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 *
 */

#include "licence_info.hpp"
#include "bcp_imp.hpp"
#include "fileview.hpp"
#include <fstream>
#include <iostream>

std::set< std::string > ignore_authors = {
    "Joe Coder",
    "https nuxi nl",
    "France",
    "Paris",
    "Grenoble",
    "UK",
    "London",
    "the Netherlands",
    "Amsterdam",
    "Poland",
    "Lodz",
    "notice to include the year",
    "LLC",
    "td",
    "and other legal information",
    "other legal information",
    "All Rights Reserved",
    "Copyright",
    "Distributed under the Boost Software License",
    "FILL SOMETHING HERE",
    "Jr",
    "Kantstrasse",
    "LLC< a",
    "alignment",
    "alignments",
    "TD",
    "Version",
    "based on code",
    "ciere com",
    "for the original version Copyright",
    "jmc",
    "mailto ichesnokov gmail com",
    "maintainer",
    "matias capeletto gmail com",
    "syntax Now understands",
    "syntax Now understands copyright",
    "yyyy yyyy",
    "it is here",
    "released to the",
    "a href",
    "III",
    "III -",
    "P",
};

std::map< std::string, std::string > replace_authors = {
    { "Oracle and or its affiliates", "Oracle and/or its affiliates" },
    { "a href http www coderage com target top CodeRage", "CodeRage LLC" },
    { "CodeRage", "CodeRage LLC" },
    { "a href http www coderage com turkanis target top Jonathan", "Jonathan Turkanis" },
    { "Adobe Systems Incorporated< span", "Adobe Systems Inc" },
    { "Adobe Systems Incorporated", "Adobe Systems Inc" },
    { "Agustin Berge", "Agustín Bergé" },
    { "Agustin K-ballo Berge", "Agustín Bergé" },
    { "Agust\xedn Berg\xe9", "Agustín Bergé" },
    { "J\xc3\x20lio Hoffimann", "Júlio Hoffimann" },
    { "AlainMiniussi", "Alain Miniussi" },
    { "Andreas Huber Doenni", "Andreas Huber Dönni" },
    { "Andrew Lumsdaine< p", "Andrew Lumsdaine" },
    { "Andrzej Krzemie", "Andrzej Krzemienski" },
    { "Andrzej", "Andrzej Krzemienski" },
    { "Antony Polukhin Move semantics implementation", "Antony Polukhin" },
    { "Author Matyas W Egyhazy", "Matyas Egyhazy" },
    { "Authors David Doria", "David Doria" },
    { "Barend", "Barend Gehrels" },
    { "Beman", "Beman Dawes" },
    { "Beman Daves", "Beman Dawes" },
    { "Beman Dawes Boost Filesystem", "Beman Dawes" },
    { "Beman Dawes nbsp", "Beman Dawes" },
    { "Benoit", "Benoit Dequidt" },
    { "Bjorn Reese", "Bjørn Reese" },
    { "Bj\xc3 rn Reese", "Bjørn Reese" },
    { "Boost Test contributors", "Boost.Test contributors", },
    { "Boost Test team", "Boost.Test contributors", },
    { "Boost test team", "Boost.Test contributors", },
    { "Boost org", "Boost.org", },
    { "Boris Schaling", "Boris Schäling" },
    { "Boris Schaeling", "Boris Schäling" },
    { "Boris Sch\xc3 ling", "Boris Schäling" },
    { "C Dean Michael Berris", "Dean Michael Berris" },
    { "C Google Inc", "Google Inc" },
    { "C Jeffrey Lee Hellrung", "Jeffrey Lee Hellrung" },
    { "Chris Uzdavinis<br", "Chris Uzdavinis" },
    { "Christoper Kohlhoff", "Christopher M Kohlhoff" },
    { "Christopher Kormanyos< p", "Christopher Kormanyos" },
    { "Copyright \xc2 John Maddock", "John Maddock" },
    { "Cortex Software GmbH< p", "Cortex Software GmbH" },
    { "CrystalClear Software", "CrystalClear Software Inc" },
    { "Daniel C Nuffer<br", "Daniel C Nuffer" },
    { "Daniel C Nuffer<br Revised", "Daniel C Nuffer" },
    { "Dave Abrahams", "David Abrahams" },
    { "David Abrahams< div", "David Abrahams" },
    { "David Abrahams< td", "David Abrahams" },
    { "David Bellot<br", "David Bellot" },
    { "Douglas Gregor< p", "Douglas Gregor" },
    { "Dr John Maddock", "John Maddock", },
    { "Edward Diener< div", "Edward Diener" },
    { "Eric Niebler< div", "Eric Niebler" },
    { "ENS Lyon< i", "ENS Lyon" },
    { "Fabian K\xc3 hler", "Fabian Köhler" },
    { "Felix H\xc3 fling", "Felix Höfling" },
    { "Fernando", "Fernando Luis Cacciola Carballal" },
    { "Fernando Cacciola", "Fernando Luis Cacciola Carballal" },
    { "Fernando Luis Cacciola Carballal< p", "Fernando Luis Cacciola Carballal" },
    { "Fernando Luis Cacciola Carballal<br Copyright", "Fernando Luis Cacciola Carballal" },
    { "Francisco Jose Tapia", "Francisco José Tapia" },
    { "Francisco Jos\xc3 Tapia", "Francisco José Tapia" },
    { "Francisco Tapia", "Francisco José Tapia" },
    { "Gennadiy Rozental Copyright", "Gennadiy Rozental" },
    { "Glen Fernandes C", "Glen Fernandes" },
    { "Glen Joseph Fernandes C", "Glen Joseph Fernandes" },
    { "Gottfried Ganssauge", "Gottfried Ganßauge" },
    { "Gottfried Gan\xc3 auge", "Gottfried Ganßauge" },
    { "Guillaume Melquiond< i", "Guillaume Melquiond" },
    { "Gunter Winkler<br", "Gunter Winkler" },
    { "Hartmut Kaiser<br", "Hartmut Kaiser" },
    { "Hartmut Kaiser< font", "Hartmut Kaiser" },
    { "Herv eacute", "Hervé Brönnimann" },
    { "Herv eacute Br ouml nnimann", "Hervé Brönnimann" },
    { "Herve Bronnimann", "Hervé Brönnimann" },
    { "Herv\xc3 Br\xc3 nnimann", "Hervé Brönnimann" },
    { "Howard", "Howard Hinnant" },
    { "Howard Hinnant Copyright", "Howard Hinnant" },
    { "Ilya Murav jov", "Ilya Murav'jov" },
    { "Institute of Transport", "Institute of Transport, Railway Construction and Operation, University of Hanover" },
    { "Intel Corporation<br", "Intel Corporation" },
    { "Ion Gazta", "Ion Gaztañaga" },
    { "Ion Gaztanaga", "Ion Gaztañaga" },
    { "Ion Gazta\xc3 aga", "Ion Gaztañaga" },
    { "Jaakko J", "Jaakko Järvi" },
    { "Jaakko J\xef rvi", "Jaakko Järvi" },
    { "Jaakko J\xe4rvi", "Jaakko Järvi" },
    { "James E King", "James E King III" },
    { "James E King III< p", "James E King III" },
    { "Jeff Westfahl<br", "Jeff Westfahl" },
    { "Jeff Westfahl< p", "Jeff Westfahl" },
    { "Jeffrey Hellrung", "Jeffrey Lee Hellrung", },
    { "Jens Mauer", "Jens Maurer" },
    { "Jeremiah J Willcock", "Jeremiah Willcock" },
    { "Jeremy Siek<br", "Jeremy Siek" },
    { "Jeremy G Siek", "Jeremy Siek" },
    { "Jeremy G Siek< P", "Jeremy Siek" },
    { "Jeremy W Murphy", "Jeremy William Murphy" },
    { "Joachim Faulhaber< p", "Joachim Faulhaber" },
    { "Joachim Faulhaber<br Copyright", "Joachim Faulhaber" },
    { "Joaqu iacute n M L oacute pez Mu ntilde oz", "Joaquín M López Muñoz" },
    { "Joaqu\xc3 n M L\xc3 pez Mu\xc3 oz", "Joaquín M López Muñoz" },
    { "Joaquin M Lopez Munoz", "Joaquín M López Muñoz" },
    { "Joaqu\xedn M L\xf3pez Mu\xf1oz", "Joaquín M López Muñoz" },
    { "Joel de Guzman<br", "Joel de Guzman" },
    { "Joel de Guzman< font", "Joel de Guzman" },
    { "Joel de Guzman<font size", "Joel de Guzman" },
    { "Joel falcou", "Joel Falcou" },
    { "Johan Rade", "Johan Råde" },
    { "Johan R\xc3 de", "Johan Råde" },
    { "John maddock", "John Maddock" },
    { "John Maddock< p", "John Maddock" },
    { "John R Bandela< i" ,"John R Bandela" },
    { "JongSoo Park", "Jong Soo Park" },
    { "Jongsoo Park", "Jong Soo Park" },
    { "Jo\xe3o Abecasis", "Joao Abecasis" },
    { "Joao Abecasis", "João Abecasis" },
    { "Jo\xc3 o Abecasis", "João Abecasis" },
    { "Juan Carlos Arevalo-Baeza<br", "Juan Carlos Arevalo-Baeza" },
    { "Just Software Solutions Ltd http www justsoftwaresolutions co uk", "Just Software Solutions Ltd" },
    { "J\xfcrgen Hunold", "Jürgen Hunold" },
    { "Juergen Hunold", "Jürgen Hunold" },
    { "K Noel Belcourt", "Noel Belcourt" },
    { "Klemens Morgenstern", "Klemens D Morgenstern" },
    { "Kohei Takahshi", "Kohei Takahashi" },
    { "Kohei Takahsahi", "Kohei Takahashi" },
    { "Kyle Lutz<br", "Kyle Lutz" },
    { "Lars Gullik Bj\xc3 nnes", "Lars Gullik Bjønnes" },
    { "Lorenzo", "Lorenzo Caminiti" },
    { "Lorenzo Caminiti< p", "Lorenzo Caminiti" },
    { "M A" , "Thijs van den Berg" },
    { "Martin", "Martin Wille" },
    { "Martin Wille<br" ,"Martin Wille" },
    { "Mathias Koch<br", "Mathias Koch" },
    { "Matthias Troyer< p", "Matthias Troyer" },
    { "Matthias Troyerk", "Matthias Troyer" },
    { "Matias", "Matias Capeletto" },
    { "Matias Capeletto< p", "Matias Capeletto" },
    { "Matyas W Egyhazy", "Matyas Egyhazy" },
    { "Niall Douglas additions for colors", "Niall Douglas" },
    { "Nicholas Edmonds", "Nick Edmonds" },
    { "Nicholas Thompson", "Nick Thompson" },
    { "Nikhar", "Nikhar Agrawal" },
    { "NVIDIA CORPORATION", "Nvidia Corporation" },
    { "Oliver Kowalke< p", "Oliver Kowalke" },
    { "Paul A", "Paul A Bristow" },
    { "Paul A Bristow - filename changes for boost-trunk", "Paul A Bristow" },
    { "Paul A Bristow Added some Quickbook snippet markers", "Paul A Bristow" },
    { "Paul A Bristow Doxygen comments changed", "Paul A Bristow" },
    { "Paul A Bristow Doxygen comments changed for new version of documentation", "Paul A Bristow" },
    { "Paul A Bristow To incorporate into Boost Math", "Paul A Bristow" },
    { "Paul A Bristow added Doxygen comments", "Paul A Bristow" },
    { "Paul A Bristow additions for more colors", "Paul A Bristow" },
    { "Paul A Bristow comments", "Paul A Bristow" },
    { "Paul A Bristow incorporated Boost Math", "Paul A Bristow" },
    { "Paul A Bristow with new tests", "Paul A Bristow" },
    { "Paul A Britow", "Paul A Bristow" },
    { "Paul Bristow", "Paul A Bristow" },
    { "Paul A Bristow< p", "Paul A Bristow" },
    { "Paul a Bristow", "Paul A Bristow" },
    { "Pearson Education Inc Reprinted with", "Pearson Education Inc" },
    { "Perforce", "Perforce Software Inc" },
    { "R W Grosse-Kunstleve", "Ralf W Grosse-Kunstleve" },
    { "Ralf W Grosse-Kunsteve", "Ralf W Grosse-Kunstleve" },
    { "Rene Rivera< p", "Rene Rivera" },
    { "Reverge Studios Inc NL", "Reverge Studios Inc" },
    { "Ross Smith<br", "Ross Smith" },
    { "Samuel Krempp< i", "Samuel Krempp" },
    { "Stephen Cleary", "Steve Cleary" },
    { "Stephen Cleary< p", "Steve Cleary" },
    { "Stephen Cleary<br Copyright", "Steve Cleary" },
    { "Steven Watanabe< p", "Steven Watanabe" },
    { "Thomas", "Thomas Heller" },
    { "Tiago de Paula Peixoto --", "Tiago de Paula Peixoto" },
    { "Tobias", "Tobias Schwinger" },
    { "Tobias Schwinger<br", "Tobias Schwinger" },
    { "Tom Westerhout font fixes to support Sphinx", "Tom Westerhout" },
    { "Trustees of Indiana University", "The Trustees of Indiana University" },
    { "Trustees of Indiana University< TD", "The Trustees of Indiana University" },
    { "Trustees of Indiana University< td", "The Trustees of Indiana University" },
    { "Vaclav Vesely<br", "Vaclav Vesely" },
    { "Vicente Botet", "Vicente J Botet Escriba" },
    { "Vicente J Botet Escrib aacute", "Vicente J Botet Escriba" },
    { "Vladimur Prus", "Vladimir Prus" },
    { "Vladimir Prusa", "Vladimir Prus" },
    { "Voipster Indrek dot Juhani at voipster dot com", "Voipster" },
    { "Wind River", "Wind River Inc", },
    { "a href http www boost org people doug gregor html Doug Gregor< a", "Doug Gregor" },
    { "a href contact html Andreas Huber", "Andreas Huber Dönni" },
    { "and Andrew Lumsdaine", "Andrew Lumsdaine" },
    { "and Andrew Lumsdaine< p", "Andrew Lumsdaine" },
    { "and Douglas Gregor", "Douglas Gregor" },
    { "and Jeremy Siek", "Jeremy Siek" },
    { "and Thomas Witt", "Thomas Witt" },
    { "and Thomas Witt --", "Thomas Witt" },
    { "and license Copyright", "Gennadiy Rozental" },
    { "andrzej Krzemienski", "Andrzej Krzemienski" },
    { "author", "Rene Rivera" },
    { "b Barend Gehrels< b", "Barend Gehrels" },
    { "b Bruno Lalande< b", "Bruno Lalande" },
    { "b Mateusz Loskot< b", "Mateusz Loskot", },
    { "boost", "Boost.org", },
    { "c Dean Michael Berris", "Dean Michael Berris" },
    { "c The Trustees of Indiana University", "The Trustees of Indiana University" },
    { "font", "Jonathan de Halleux" },
    { "font color", "Jonathan de Halleux" },
    { "iamvfx gmail com", "iamvfx@gmail.com" },
    { "nathan Ridge", "Nathan Ridge" },
    { "nbsp Vinnie nbsp Falco", "Vinnie Falco" },
    { "nbsp Vinnie nbsp Falco<br", "Vinnie Falco" },
    { "ohn Maddock", "John Maddock" },
    { "section", "Jeff Garland" },
    { "span", "Christophe Henry" },
    { "ulink url http vladimirprus com Vladimir", "Vladimir Prus" },
    { "tf ttqv com", "Thomas Flemming" },
    { "warmerdam pobox com", "Frank Warmerdam" },
    { "webbot bot Timestamp s-type EDITED s-format Y startspan", "Andreas Huber Dönni" },
};

std::map< std::string, std::set< std::string > > replace_authors_multiple = {
    { "Howard Hinnant amp John Maddock", { "Howard Hinnant", "John Maddock" } },
    { "Mathias Koch David Bellot", { "Mathias Koch", "David Bellot" } },
    { "Nat Goodspeed Oliver Kowalke", { "Oliver Kowalke", "Nat Goodspeed" } },
    { "Oliver Kowalke Nat Goodspeed", { "Oliver Kowalke", "Nat Goodspeed" } },
    { "i", { "Jeremy Siek", "Andrew Lumsdaine" } },
    { "David Abrahams Jeremy Siek Thomas Witt", { "David Abrahams", "Jeremy Siek", "Thomas Witt" } },
    { "David Abrahams Steve Cleary", { "David Abrahams", "Steve Cleary" } },
    { "Edward Nevill Oliver Kowalke", { "Edward Nevill", "Oliver Kowalke" } },
};

std::map< std::string, std::string > replace_licenses = {
    { "Boost Software License, Version 1.0 (variant #1)", "Boost Software License, Version 1.0" },
    { "Boost Software License, Version 1.0 (variant #2)", "Boost Software License, Version 1.0" },
};

const int boost_license_lines = 3;
static const std::string boost_license_text[boost_license_lines] = {
  "Distributed under the Boost Software License, Version 1.0. (See",
  "accompanying file LICENSE_1_0.txt or copy at",
  "http://www.boost.org/LICENSE_1_0.txt)"
};

fileview::const_iterator
context_before_license(const fileview& v, fileview::const_iterator start,
                       int context_lines = 3)
{
  char last_char = '\0';
  while (start != v.begin() && context_lines >= 0) {
    if (((*start == '\r') || (*start == '\n'))
        && ((last_char == *start) || ((last_char != '\r') && (last_char != '\n'))))
        --context_lines;

    last_char = *start;
    --start;
  }

  // Unless we hit the beginning, we need to step forward one to start
  // on the next line.
  if (start != v.begin()) ++start;

  return start;
}

fileview::const_iterator
context_after_license(const fileview& v, fileview::const_iterator end,
                      int context_lines = 3)
{
  char last_char = '\0';
  while (end != v.end() && context_lines >= 0) {
    if ((*end == '\r' || *end == '\n')
        && (last_char == *end || (last_char != '\r' && last_char != '\n')))
        --context_lines;

    last_char = *end;
    ++end;
  }

  return end;
}

static std::string
find_prefix(const fileview& v, fileview::const_iterator start_of_line)
{
  while (start_of_line != v.begin()
         && *start_of_line != '\n'
         && *start_of_line != '\r')
    --start_of_line;
  if (start_of_line != v.begin())
    ++start_of_line;

  fileview::const_iterator first_noncomment_char = start_of_line;
  while (*first_noncomment_char == '/'
         || *first_noncomment_char == '*'
         || *first_noncomment_char == ' '
         || *first_noncomment_char == '#')
    ++first_noncomment_char;

  return std::string(start_of_line, first_noncomment_char);
}

static std::string
html_escape(fileview::const_iterator first, fileview::const_iterator last)
{
  std::string result;
  while (first != last) {
    switch (*first) {
    case '<': result += "&lt;"; break;
    case '>': result += "&gt;"; break;
    case '&': result += "&amp;"; break;
    default: result += *first;
    }
    ++first;
  }
  return result;
}

static bool is_non_bsl_license(int index)
{
  return index > 2;
}

void bcp_implementation::scan_license(const fs::path& p, const fileview& v)
{
   std::pair<const license_info*, int> licenses = get_licenses();
   //
   // scan file for all the licenses in the list:
   //
   int license_count = 0;
   int author_count = 0;
   int nonbsl_author_count = 0;
   bool has_non_bsl_license = false;
   fileview::const_iterator start_of_license = v.begin(),
                            end_of_license = v.end();
   bool start_in_middle_of_line = false;

   m_file_data[p];

   for(int i = 0; i < licenses.second; ++i)
   {
      boost::match_results<fileview::const_iterator> m;
      if(boost::regex_search(v.begin(), v.end(), m, licenses.first[i].license_signature))
      {
           start_of_license = m[0].first;
         end_of_license = m[0].second;

         if (is_non_bsl_license(i) && i < licenses.second - 1)
           has_non_bsl_license = true;

         // add this license to the list:
         m_license_data[i].files.insert(p);
         auto lic_it = replace_licenses.find(licenses.first[i].license_name);
         if (lic_it != replace_licenses.end()) {
             m_file_data[p].licenses.insert(lic_it->second);
         } else {
             m_file_data[p].licenses.insert(licenses.first[i].license_name);
         }
         ++license_count;
         //
         // scan for the associated copyright declarations:
         //
         boost::regex_iterator<const char*> cpy(v.begin(), v.end(), licenses.first[i].copyright_signature);
         boost::regex_iterator<const char*> ecpy;
         while(cpy != ecpy)
         {
#if 0
             // Not dealing with copyrights because we don't have the years
            if ((*cpy)[0].first < start_of_license)
              start_of_license = (*cpy)[0].first;
            if ((*cpy)[0].second > end_of_license)
              end_of_license = (*cpy)[0].second;
#endif

            /*std::cerr << "Matched: " << cpy->str() << std::endl;
            std::cerr << "After mangling: " << cpy->format(licenses.first[i].copyright_formatter, boost::format_all) << std::endl;*/
            // extract the copy holders as a list:
            std::string author_list = cpy->format(licenses.first[i].copyright_formatter, boost::format_all);
            // now enumerate that list for all the names:
            static const boost::regex author_separator("(?:\\s*,(?!\\s*(?:inc|ltd)\\b)\\s*|\\s+(,\\s*)?(and|&)\\s+)|by\\s+", boost::regex::perl | boost::regex::icase);
            boost::regex_token_iterator<std::string::const_iterator> atr(author_list.begin(), author_list.end(), author_separator, -1);
            boost::regex_token_iterator<std::string::const_iterator> eatr;
            while(atr != eatr)
            {
               // get the reformatted authors name:
               std::string name = format_authors_name(*atr);
               // add to list of authors for this file:
               if(name.size() && name[0] != '-' && ignore_authors.find(name) == ignore_authors.end())
               {
                  auto repl_it = replace_authors.find(name);
                  if (repl_it != replace_authors.end()) {
                      name = repl_it->second;
                  }
                  auto repl_mult_it = replace_authors_multiple.find(name);
                  if (repl_mult_it != replace_authors_multiple.end()) {
                      for (const auto &name : repl_mult_it->second) {
                          m_license_data[i].authors.insert(name);
                          m_file_data[p].authors.insert(name);
                          // add file to author index:
                          m_author_data[name].insert(p);
                          ++author_count;
                      }
                  } else {
                      m_license_data[i].authors.insert(name);
                      m_file_data[p].authors.insert(name);
                      // add file to author index:
                      m_author_data[name].insert(p);
                      ++author_count;
                  }

                  // If this is not the Boost Software License (license 0), and the author hasn't given
                  // blanket permission, note this for the report.
                  if (has_non_bsl_license
                      && m_bsl_authors.find(name) == m_bsl_authors.end()) {
                    ++nonbsl_author_count;
                    m_authors_for_bsl_migration.insert(name);
                  }
               }
               ++atr;
            }
            ++cpy;
         }

         while (start_of_license != v.begin()
                && *start_of_license != '\r'
                && *start_of_license != '\n'
                && *start_of_license != '.')
           --start_of_license;

         if (start_of_license != v.begin()) {
           if (*start_of_license == '.')
             start_in_middle_of_line = true;
           ++start_of_license;
         }

         while (end_of_license != v.end()
                && *end_of_license != '\r'
                && *end_of_license != '\n')
           ++end_of_license;
      }
   }
   if(license_count == 0)
      m_unknown_licenses.insert(p);
   if(license_count && !author_count)
      m_unknown_authors.insert(p);

   if (has_non_bsl_license) {
     bool converted = false;
     if (nonbsl_author_count == 0
         && license_count == 1) {
       // Grab a few lines of context
       fileview::const_iterator context_start =
         context_before_license(v, start_of_license);
       fileview::const_iterator context_end =
         context_after_license(v, end_of_license);

       // TBD: For files that aren't C++ code, this will have to
       // change.
       std::string prefix = find_prefix(v, start_of_license);

       // Create enough information to permit manual verification of
       // the correctness of the transformation
       std::string before_conversion =
         html_escape(context_start, start_of_license);
       before_conversion += "<b>";
       before_conversion += html_escape(start_of_license, end_of_license);
       before_conversion += "</b>";
       before_conversion += html_escape(end_of_license, context_end);

       std::string after_conversion =
         html_escape(context_start, start_of_license);
       if (start_in_middle_of_line)
         after_conversion += '\n';

       after_conversion += "<b>";
       for (int i = 0; i < boost_license_lines; ++i) {
         if (i > 0) after_conversion += '\n';
         after_conversion += prefix + boost_license_text[i];
       }
       after_conversion += "</b>";
       after_conversion += html_escape(end_of_license, context_end);

       m_converted_to_bsl[p] =
         std::make_pair(before_conversion, after_conversion);

       // Perform the actual conversion
       if (m_bsl_convert_mode) {
          try{
             std::ofstream out((m_boost_path / p).string().c_str());
            if (!out) {
               std::string msg("Cannot open file for license conversion: ");
               msg += p.string();
               std::runtime_error e(msg);
               boost::throw_exception(e);
            }

            out << std::string(v.begin(), start_of_license);
            if (start_in_middle_of_line)
               out << std::endl;

            for (int j = 0; j < boost_license_lines; ++j) {
               if (j > 0) out << std::endl;
               out << prefix << boost_license_text[j];
            }
            out << std::string(end_of_license, v.end());

            converted = true;
       }
       catch(const std::exception& e)
       {
          std::cerr << e.what() << std::endl;
       }
      }
     }

     if (!converted) {
       if (nonbsl_author_count > 0) m_cannot_migrate_to_bsl.insert(p);
       else m_can_migrate_to_bsl.insert(p);
     }
   }
}

